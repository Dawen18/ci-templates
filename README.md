# CI templates

Templates for GitLab CI

## Usage
Include template like below
```yaml
include:
  - project: 2cs/ci-templates
    file: Rust.gitlab-ci.yml
```

The majority of templates need `CCS_DOCKER_REGISTRY` env var to be set. 
By default, set this var to `registry.gitlab.com/2cs/docker-images` in GitLab CI/CD configuration.